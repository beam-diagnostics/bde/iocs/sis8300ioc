###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-SIS16")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-006")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-6")
epicsEnvSet("EVR_NAME", "LAB-EVR-016:")
###############################################################################
